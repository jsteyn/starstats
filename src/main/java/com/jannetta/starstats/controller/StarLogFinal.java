package com.jannetta.starstats.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author Jannetta S Steyn A class holding the parsed contents of a STAR
 *         Log.final.out file
 *
 */
public class StarLogFinal {
	private String sampleID;
	private String startjob; // date
	private String startmapping; // date
	private String finished; // date
	private double mappingspeed; //
	private int readnumber;
	private int avgreadlength;
	private int uniquemappedreadsnumber;
	private double uniquemappedreadsperc;
	private double avemappedlength;
	private int splicetotal;
	private int spliceanno;
	private int splicegtag;
	private int splicegcag;
	private int spliceatac;
	private int splicenoncanonical;
	private double misrateperbase;
	private double delrateperbase;
	private double delaveragelength;
	private double insrateperbase;
	private double insaveragelength;
	private int mappedmultiloc;
	private double mappedpercmultiloci;
	private int mappedtoomanyloci;
	private double mappedperctoomanyloci;
	private double unmappedperctoomanymis;
	private double unmappedperctooshort;
	private double unmappedpercother;
	private int chimericreads;
	private double chimericreadsperc;

	/**
	 * Constructor: Reads the Log.final.out file in the specified path and
	 * parses it into class variables
	 * 
	 * @param path
	 *            The path to the sample sub directory that holds the
	 *            Log.final.out file
	 */
	public StarLogFinal(String path, String subdir) {
		try {
			String[] vals = new String[29];
			int i = 0;
			File fn = new File(path + "/" + subdir + "/Log.final.out");
			Scanner sc = new Scanner(fn);
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String[] tokens = line.split("\t");
				if (tokens.length == 2) {
					vals[i] = tokens[1];
					i++;
				}
			}
			setVars(vals, fn);
			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Sets the local variables to the values parsed from the Log.final.out
	 * files
	 * 
	 * @param tokens
	 *            contains string values for all values read from the
	 *            Log.final.out file
	 */
	public void setVars(String[] tokens, File fn) {
		sampleID = fn.getPath();
		startjob = tokens[0];
		startmapping = tokens[1];
		finished = tokens[2];
		mappingspeed = Double.valueOf(tokens[3]);
		readnumber = Integer.valueOf(tokens[4]);
		avgreadlength = Integer.valueOf(tokens[5]);
		uniquemappedreadsnumber = Integer.valueOf(tokens[6]);
		uniquemappedreadsperc = Double.valueOf(tokens[7].substring(0, tokens[7].length() - 2));
		avemappedlength = Double.valueOf(tokens[8]);
		splicetotal = Integer.valueOf(tokens[9]);
		spliceanno = Integer.valueOf(tokens[10]);
		splicegtag = Integer.valueOf(tokens[11]);
		splicegcag = Integer.valueOf(tokens[12]);
		spliceatac = Integer.valueOf(tokens[13]);
		splicenoncanonical = Integer.valueOf(tokens[14]);
		misrateperbase = Double.valueOf(tokens[15].substring(0, tokens[15].length() - 2));
		delrateperbase = Double.valueOf(tokens[16].substring(0, tokens[15].length() - 2));
		delaveragelength = Double.valueOf(tokens[17].substring(0, tokens[16].length() - 2));
		insrateperbase = Double.valueOf(tokens[18].substring(0, tokens[18].length() - 2));
		insaveragelength = Double.valueOf(tokens[19].substring(0, tokens[18].length() - 2));
		mappedmultiloc = Integer.valueOf(tokens[20].substring(0, tokens[19].length() - 2));
		mappedpercmultiloci = Double.valueOf(tokens[21].substring(0, tokens[21].length() - 2));
		mappedtoomanyloci = Integer.valueOf(tokens[22]);
		mappedperctoomanyloci = Double.valueOf(tokens[23].substring(0, tokens[23].length() - 2));
		unmappedperctoomanymis = Double.valueOf(tokens[24].substring(0, tokens[24].length() - 2));
		unmappedperctooshort = Double.valueOf(tokens[25].substring(0, tokens[25].length() - 2));
		unmappedpercother = Double.valueOf(tokens[26].substring(0, tokens[26].length() - 2));
		if (tokens.length > 29) {
			chimericreads = Integer.valueOf(tokens[27]);
			chimericreadsperc = Double.valueOf(tokens[28].substring(0, tokens[28].length() - 2));
		} else {
			chimericreads = 0;
			chimericreadsperc = 0;
		}
	}


	/**
	 * Print the values read from the Log.final.out file as tab separated
	 * without the labels
	 */
	public void printStats() {
		System.out.print(startjob + "\t");
		System.out.print(startmapping + "\t");
		System.out.print(finished + "\t");
		System.out.print(mappingspeed + "\t");
		System.out.print(readnumber + "\t");
		System.out.print(avgreadlength + "\t");
		System.out.print(uniquemappedreadsnumber + "\t");
		System.out.print(uniquemappedreadsperc + "\t");
		System.out.print(avemappedlength + "\t");
		System.out.print(splicetotal + "\t");
		System.out.print(spliceanno + "\t");
		System.out.print(splicegtag + "\t");
		System.out.print(splicegcag + "\t");
		System.out.print(spliceatac + "\t");
		System.out.print(splicenoncanonical + "\t");
		System.out.print(misrateperbase + "\t");
		System.out.print(delrateperbase + "\t");
		System.out.print(delaveragelength + "\t");
		System.out.print(insrateperbase + "\t");
		System.out.print(insaveragelength + "\t");
		System.out.print(mappedmultiloc + "\t");
		System.out.print(mappedpercmultiloci + "\t");
		System.out.print(mappedtoomanyloci + "\t");
		System.out.print(mappedperctoomanyloci + "\t");
		System.out.print(unmappedperctoomanymis + "\t");
		System.out.print(unmappedperctooshort + "\t");
		System.out.print(unmappedpercother + "\t");
		System.out.print(chimericreads + "\t");
		System.out.println(chimericreadsperc);
	}

	/**
	 * Return the stats values as one long string with the values separated by
	 * tabs
	 * 
	 * @return String
	 */
	public String getStatsString() {
		return startjob + "\t" + startmapping + "\t" + finished + "\t" + mappingspeed + "\t" + readnumber + "\t"
				+ avgreadlength + "\t" + uniquemappedreadsnumber + "\t" + uniquemappedreadsperc + "\t" + avemappedlength
				+ "\t" + splicetotal + "\t" + spliceanno + "\t" + splicegtag + "\t" + splicegcag + "\t" + spliceatac
				+ "\t" + splicenoncanonical + "\t" + misrateperbase + "\t" + delrateperbase + "\t" + delaveragelength
				+ "\t" + insrateperbase + "\t" + insaveragelength + "\t" + mappedmultiloc + "\t" + mappedpercmultiloci
				+ "\t" + mappedtoomanyloci + "\t" + mappedperctoomanyloci + "\t" + unmappedperctoomanymis + "\t"
				+ unmappedperctooshort + "\t" + unmappedpercother + "\t" + chimericreads + "\t" + chimericreadsperc;

	}
}
