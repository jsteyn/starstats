package com.jannetta.starstats.main;

import java.util.ArrayList;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;

import com.jannetta.starstats.view.Main;

/**
 * @author Jannetta S Steyn
 * 
 */
public class StarStats {

	public static void main(String[] args) {
		Options options = new Options();
		CommandLineParser parser = new DefaultParser();
		String directory = "";
		String subdir = "";
		String outfile = "StarStats.txt";
		String pathname = "";
		String samplenames = "";
		Option dir = Option.builder("d")
				.required(true)
				.hasArg()
				.desc("Directory where STAR directories are.")
				.build();
		Option out = Option.builder("o")
				.hasArg()
				.desc("File to write the summary to. (Default is com.jannetta.starstats.main.StarStats.txt "
						+ "in the current directory")
				.build();
		Option sub = Option.builder("s")
				.hasArg()
				.desc("If your STAR log files are in a sub-directory below the sample directory, you can specify "
						+ "it here. Do not include the sample directory. The -d option will find all the sample "
						+ "directories and the directory structure that you specify with this swith will be appended "
						+ "to that.")
				.build();
		Option path = Option.builder("p")
				.hasArg()
				.desc("You can use this switch to specify a path name that will be added to the front of the "
						+ "sample directory. If this switch is not used the current directory will be used and "
						+ "the program will look for all subdirectories in the current directory and use those "
						+ "directories' names as sample names.")
				.build();
		Option file = Option.builder("f")
				.hasArg()
				.desc("If this option is specified the sample names will be read from the provided filename. If "
						+ "it is not provided a list of sub-directories will be obtained and those will be used "
						+ "as sample names. This option is useful if you have directories other than the star "
						+ "output in the directory containing the star output directories.")
				.build();
		options.addOption(dir);
		options.addOption(out);
		options.addOption(sub);
		options.addOption(path);
		options.addOption(file);
		System.out.println("Extracting stats from: ");
		ArrayList<String> filenames;
		try {
			CommandLine cmd = parser.parse(options, args);
			if (cmd.hasOption("d")) {
				directory = cmd.getOptionValue("d");
			}
			if (cmd.hasOption("o")) {
				outfile = cmd.getOptionValue("o");
			}
			if (cmd.hasOption("s")) {
				subdir = cmd.getOptionValue("s");
			}
			if (cmd.hasOption("p")) {
				pathname = cmd.getOptionValue("p");
			}
			if (cmd.hasOption("f")) {
				samplenames = cmd.getOptionValue("f");
				 filenames = Main.getDirectories(directory, samplenames);
			} else {
				filenames = Main.getDirectories(directory);
			}
			for (int i = 0; i < filenames.size();i++) {
				System.out.println(filenames.get(i) + "/" + subdir + "/" + "Log.final.out");
			}
			Main.extractStats(directory, subdir, outfile, pathname, samplenames);
			System.out.println("Done.");
		} catch (MissingOptionException e) {
			help(options);
		} catch (UnrecognizedOptionException e) {
			help(options);
		} catch (MissingArgumentException e) {
			System.out.println("You need to provide a value for -" + e.getOption().getOpt());
		} catch (ParseException e) {
			System.err.println("Parsing failed.  Reason: " + e.getMessage());
			e.printStackTrace();
		}

	}
	
	/**
	 * Prints the help text explaining command line parameters
	 * @param options 
	 */
	private static void help(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("java -jar StarStats.jar\n"
				+ "Version: 1.2\n"
				+ "Program for summarising STAR stats of all sample files into one tab delimited file.", options);
	}
	
}

