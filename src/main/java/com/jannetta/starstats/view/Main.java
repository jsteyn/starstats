package com.jannetta.starstats.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import com.jannetta.starstats.controller.StarLogFinal;

/**
 * @author Jannetta S Steyn
 *
 *         Program for summarising STAR stats of all sample files into one tab
 *         delimited file 2017/03/14 Su-Yang moved the main class into package
 *         com.jannetta.starstats.main 2017/03/15 Rachel tested and it is not
 *         working with her directory structure. Need to make changes to
 *         acommodate different directory structure.
 */
public class Main {

	public static void extractStats(String directory, String subdir, String outfile, String pathname,
			String filenames) {
		ArrayList<String> dirs;
		if (filenames.equals("") || filenames == null) {
			dirs = getDirectories(directory);
		} else {
			dirs = getDirectories(directory, filenames);
		}
		try {
			PrintWriter po = new PrintWriter(new File(outfile));
			printHeader(po);
			for (int i = 0; i < dirs.size(); i++) {
				StarLogFinal log = new StarLogFinal(dirs.get(i), subdir);
				String[] sample = dirs.get(i).split("/");
				po.print(sample[sample.length - 1] + "\t");
				po.println(log.getStatsString());

			}
			po.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Gets all directories in the path specified and return them as ArrayList
	 * 
	 * @param path
	 * @return an ArrayList of String containing the directories in the
	 *         specified path
	 */
	public static ArrayList<String> getDirectories(String path) {
		ArrayList<String> dir = new ArrayList<String>();
		File[] directories = new File(path).listFiles(File::isDirectory);
		for (File f : directories) {
			dir.add(f.getAbsolutePath());
		}
		return dir;
	}

	public static ArrayList<String> getDirectories(String path, String samplenames) {
		ArrayList<String> dir = new ArrayList<String>();
		Scanner sc;
		try {
			sc = new Scanner(new File(samplenames));
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				dir.add(path + "/" + line);
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return dir;
	}

	private static void printHeader(PrintWriter po) {
		String[] firstheader = { "\t\t\t\t\t\t\tUNIQUE READS:", "\t\t\t\t\t\t\t\t\t\t\t\t\t\tMULTI-MAPPING READS:",
				"\t\t\t\tUNMAPPED READS:", "\t\t\tCHIMERIC READS:\n" };
		String[] titles = { "Sample_ID", "Started job on", "Started mapping on", "Finished on",
				"Mapping speed: Million of reads per hour", "Number of input reads", "Average input read length",
				"Uniquely mapped reads number", "Uniquely mapped reads %", "Average mapped length",
				"Number of splices: Total", "Number of splices: Annotated (sjdb)", "Number of splices: GT/AG",
				"Number of splices: GC/AG", "Number of splices: AT/AC", "Number of splices: Non-canonical",
				"Mismatch rate per base, %", "Deletion rate per base |", "Deletion average length",
				"Insertion rate per base", " Insertion average length", "Number of reads mapped to multiple loci",
				"% of reads mapped to multiple loci", "Number of reads mapped to too many loci",
				"% of reads mapped to too many loci", "% of reads unmapped: too many mismatches",
				"% of reads unmapped: too short", "% of reads unmapped: other", "Number of chimeric reads",
				"% of chimeric reads" };
		for (int i = 0; i < firstheader.length; i++) {
			po.print(firstheader[i]);
		}
		for (int i = 0; i < titles.length; i++) {
			if (i < titles.length - 1) {
				po.print(titles[i] + "\t");
			} else {
				po.println(titles[i]);
			}
		}
	}
}
